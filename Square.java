package asg06;

public class Square extends Expression {
    
	protected Expression expression;
	
	public Square(Expression sq){
		expression=sq;

	}
	
	public int evaluate(){
		return expression.evaluate() * expression.evaluate();
	}
	
	public String toString()
	   {
		   
	     return String.format("%d *  %d =  %d", expression.evaluate() ,expression.evaluate() , evaluate()  );
	   } 
}
