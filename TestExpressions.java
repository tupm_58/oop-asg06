package asg06;

public class TestExpressions {

	public static void main(String[] args) {
		 Expression two = new Numeral(2);
         System.out.println(two.toString());
         
         Expression twoSquare=new Square(two);
         System.out.println(twoSquare.toString());
         
         Expression one=new Numeral(1);
         Expression twoPlusTwoSquare=new Addition(one,twoSquare);
         Expression twoSubtractTwoSquare=new Subtraction(one,twoSquare);
         Expression twoTimesTwoSquare=new Multiplication(one,twoSquare);
         System.out.println(twoPlusTwoSquare.toString());
         System.out.println(twoTimesTwoSquare.toString());
         
         
         Expression threeSquare = new Square(new Numeral(3));
         Expression threeSquarePlusOne = new Addition(threeSquare, one);
         Expression theBigSquare = new Square(threeSquarePlusOne);
         System.out.println(theBigSquare.toString());
	}

}
