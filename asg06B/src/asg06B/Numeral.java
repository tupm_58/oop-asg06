package asg06B;


public class Numeral extends Expression {
	protected int value;
	
	public Numeral(int x){
		value=x;
	}

	public int evaluate(){
		return value;
	}

	public String toString()
	   {
		   
	      return String.format("%d ",evaluate()   );
	   } 
}
