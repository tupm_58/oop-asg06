package asg06B;

abstract class Expression {
 

	public abstract String toString();
		
	
	public abstract int evaluate();
	
}
