package asg06B;

 class Multiplication extends BinaryExpression {
	
	protected Expression left;
	protected Expression right;
	
	public Multiplication(Expression a, Expression b){
		
		left=a;
		right=b;
		
	}
	
	public int evaluate(){
		return left.evaluate()*right.evaluate();
	}
	
	public String toString()
	   {
		   
	      return String.format("%d *  %d =  %d",left.evaluate() ,right.evaluate() ,evaluate() );
	   } 
	
	@Override
	Expression left() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	Expression right() {
		// TODO Auto-generated method stub
		return null;
	} 


}
