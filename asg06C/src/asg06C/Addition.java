package asg06C;


public class Addition implements BinaryExpression {
	
	protected Expression left;
	protected Expression right;
	
	public Addition(Expression a, Expression b){
		
		left=a;
		right=b;
		
	}
	
	public int evaluate(){
		return left.evaluate()+right.evaluate();
	}
	public String toString()
	   {
		   
	      return String.format("%d +  %d =  %d",left.evaluate() ,right.evaluate() ,evaluate() );
	   }

	@Override
	public Expression left() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Expression right() {
		// TODO Auto-generated method stub
		return null;
	}


}
