package asg06C;


public class Numeral implements Expression {
	protected int value;
	
	public Numeral(int x){
		value=x;
	}

	public int evaluate(){
		return value;
	}

	public String toString()
	   {
		   
	      return String.format("%d ",evaluate()   );
	   } 
}
