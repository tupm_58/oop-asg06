package asg06C;

public interface Expression {
 

	public  String toString();
		
	
	public  int evaluate();
	
}
