package asg06;

public class Addition extends BinaryExpression {
	
	protected Expression left;
	protected Expression right;
	
	public Addition(Expression a, Expression b){
		
		left=a;
		right=b;
		
	}
	
	public int evaluate(){
		return left.evaluate()+right.evaluate();
	}
	public String toString()
	   {
		   
	      return String.format("%d +  %d =  %d",left.evaluate() ,right.evaluate() ,evaluate() );
	   } 

}
